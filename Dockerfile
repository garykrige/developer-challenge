FROM node:8-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY src/package.json /usr/src/app/
RUN yarn install

COPY src /usr/src/app/

EXPOSE 3000
CMD ["node", "./bin/www"]
