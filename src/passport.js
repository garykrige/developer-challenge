var LocalStrategy = require('passport-local').Strategy;

var User = require('./models/user');

module.exports = function(passport) {

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  passport.use('local-login', new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    function(req, email, password, done) {
      process.nextTick(function() {
        User.findOne({
          'local.email': email
        }, function(err, user) {
          if (err) return done(err);

          if (!user) {
            return done(null, false, req.flash('loginMessage', 'No user found.'));
          }
          if (!user.validPassword(password)) {
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
          }

          return done(null, user);
        });
      });

    }));

  passport.use('local-signup', new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    function(req, email, password, done) {
      process.nextTick(function() {
        if (!req.user) {
          User.findOne({
            'local.email': email
          }, function(err, user) {
            if (err) return done(err);

            if (user) {
              return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            }

            if (password.length < 5) {
              return done(null, false, req.flash('signupMessage', 'Password must be at least 5 characters.'));
            }

            var newUser = new User();

            newUser.local.email = email;
            newUser.local.password = newUser.generateHash(password);

            newUser.save(function(err) {
              if (err) return done(err);

              return done(null, newUser);
            });

          });
        }

        return done(null, req.user);

      });

    }));

};
