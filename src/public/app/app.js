(function() {

  angular
    .module('futureMedia', [
      'ngRoute'
    ])
    .config(config);

  config.$inject = ['$routeProvider', '$httpProvider'];

  function config($routeProvider, $httpProvider) {

    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });

    $httpProvider.interceptors.push('authInterceptor');

  }

  angular
    .module('futureMedia')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$rootScope', '$q'];

  function authInterceptor($rootScope, $q) {

    return {
      request: function(config) {
        config.headers = config.headers || {};
        return config;
      },

      responseError: function(response) {
        return $q.reject(response);
      }
    };
  }


})();
