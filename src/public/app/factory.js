(function() {

  angular
    .module('boilerplate')
    .factory('getDataFromAPI', getDataFromAPI);

  getDataFromAPI.$inject = ['$http'];

  function getDataFromAPI($http) {

    return {
      loadData: loadData
    };

    return $http({
      method: 'GET',
      url: API.url + '/profile',
      params: {}
    }).then(function(data) {
      return data.body;
    })
    .catch(function(error) {
      return error;
    });
  }


})();
