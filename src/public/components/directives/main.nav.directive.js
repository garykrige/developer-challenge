(function() {
  angular
    .module('boilerplate')
    .directive('mainNav', tinMainNav);

  function tinMainNav() {

    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'components/directives/navbar.html'
    };

    return directiveDefinitionObject;
  }

})();
