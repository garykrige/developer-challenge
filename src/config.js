'use strict';

const config = {
  LOG_LEVEL: process.env.LOG_LEVEL || 'info',
  DATABASE_URL: process.env.DATABASE_URL || 'mongodb://localhost/mydb',
  SECRET_KEY: process.env.SECRET_KEY || 'veryverysecretkey'
};

module.exports = config;
