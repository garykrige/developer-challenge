# Developer Challenge

## Overview
Develop a simple web application with basic user information using our preferred technology stack.

## Process
Please create an account on bitbucket.org. We use GIT for our source control and would like you to use it regularly during your developer challenge. The more you commit, the better since I can track the code changes over time.

## Technology Stack
Your primary coding language will be JavaScript - we use it on the server-side as well (node.js). The stack is called MEAN for MongoDB, Express.js Angular.js and Node.js. You have to use these technologies together with HTML and CSS.

I highly recommend you look into the following libraries and frameworks to help you:
Grunt, Bootstrap v3, Angular-Bootstrap, Angular-Resource, Mongoose.js, Passport.js, Videogular

## Detail
You need to deliver a node application that I can clone from your bitbucket repo and run it locally.  

The website has the following:
- Landing page
  - with a short looping video background that scales optimally based on screen size
  - Sign in and sign up links
  - Sign in block opens on this page
- Sign up (also on landing page)
  - Captures the following: firstname, surname, email, favourite color (choose from drop down or add new), password (needs to be at least 5 characters long), profile picture
- Top navbar (show only once logged in)
  - Links to ‘sign out’, my profile, home
- My Profile
  - Basically the same fields as sign up except password (you can update these details)
  - Password reset action (sends an email from server side with a link that can be followed
- Home
  - List of last logins by the current user (essentially a report)
  - Ability to page and filter this report (server-side paging)

That’s it
